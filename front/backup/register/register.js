import React from 'react'
import axios from 'axios'
import * as yup from 'yup'

class Register extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            confirmPassword: ''}        
        this.submit = this.submit.bind(this)
        this.inputChange = this.inputChange.bind(this)

      
    }
    
    submit(event) {
      event.preventDefault();

      const validationSchema = yup.object().shape({
        email: yup.string()
          .email('E-mail is not valid!')
          .required('E-mail is required!'),
        name: yup.string()
          .required('Name is required'),
        password: yup.string()
          .required('Password Required'),
        confirmPassword: yup.string()
          .oneOf([yup.ref('password'), null], 'Passwords must match')
      })

      var userObject = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password
      }

      var errors = {
        name: '',
        email: '',
        password: '',
        confirmPassword: ''
      }

      // let formValid = false;

      validationSchema
        .validate({ 
          name: this.state.name,
          email: this.state.email,
          password: this.state.password,
          confirmPassword: this.state.confirmPassword
        })
        .then(function(data) {
          console.log(userObject)
          
          // axios request to node
          // axios.post('http://localhost:4000/addUser', userObject)
          // .then((response) => {
          //   if(response.data === "error")
          //     console.log("error")
          //   else
          //     console.log("success")
          // }).catch(err => {
          //   console.error(err)
          // }); //axios ends

        })
        .catch(function(err) {
          console.log(err.value) // => 'ValidationError'
          errors = err
        });
    }

    

    inputChange(event) {
        if(event.target.name === "name")
        this.setState({name : event.target.value})

        if(event.target.name === "email")
        this.setState({email : event.target.value})
        
        if(event.target.name === "password")
        this.setState({password : event.target.value})

        if(event.target.name === "confirmPassword")
        this.setState({confirmPassword : event.target.value})
    }

    render() {
      return (
        <form onSubmit={this.submit}>
            <h1>Register</h1>
            <p>Register Yourself</p>
            <input
            type="text"
            name="name"
            onChange={this.inputChange}
            placeholder = {'Enter your name'}
            value = {this.state.username}
            /><br/>
            <p>{}</p>
            <br/>

            <input
            type="text"
            name="email"
            onChange={this.inputChange}
            placeholder = {'Enter your email'}
            value = {this.state.email}
            /><br/>
            
            <input
            type="password"
            name="password"
            onChange={this.inputChange}
            placeholder = {'Enter your password'}
            value = {this.state.password}
            /><br/>

            <input
            type="password"
            name="confirmPassword"
            onChange={this.inputChange}
            placeholder = {'Comfirm password'}
            // value = {this.state.username}
            /><br/>

            <button type="submit">Submit</button>
        </form>
      );

    }
  }

export default Register;