import React from 'react'

import Navbar from './components/navbar/navbar'
import Login from './pages/login/login'
import Register from './pages/register/register'
// Router Libs
import { BrowserRouter, Route, Switch } from "react-router-dom"
import Dashboard from './pages/dashboard/dashboard'

function App() {
  return (
    <div className="App">
      <Navbar />
      <BrowserRouter>
        <div>
          <Switch>
              <Route path='/login' component={Login} />
              <Route path='/register' component={Register} />
              <Route path='/dashboard' component={Dashboard} />
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App
