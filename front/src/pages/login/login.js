import React from 'react'

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {email: '', password: ''}        
        this.submit = this.submit.bind(this)
        this.inputChange = this.inputChange.bind(this)
    }
    
    submit(event) {
        event.preventDefault();
        console.log(this.state)
    }

    inputChange(event) {
        if(event.target.name === "email")
        this.setState({email : event.target.value})
        
        if(event.target.name === "password")
        this.setState({password : event.target.value})
        // console.log(this.state)
    }

    render() {
      return (
        <form onSubmit={this.submit}>
            <h1>Login</h1>
            <p>Enter your name:</p>
            <input
            type="text"
            name="email"
            onChange={this.inputChange}
            placeholder = {'Enter your name'}
            value = {this.state.email}
            />
            <br />
            <input
            type="password"
            name="password"
            onChange={this.inputChange}
            placeholder = {'Enter your password'}
            value = {this.state.password}
            />
            <button type="submit">Submit</button>
        </form>
      );

    }
  }

export default Login;