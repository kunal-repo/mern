import React from 'react'
import './register.css'
import { Formik, Field, Form, ErrorMessage } from 'formik';
import axios from 'axios'
import * as Yup from 'yup'

class Register extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
          name: '',
          email: '',
          password: '',
          confirmPassword: ''}        
    }

    setLocalToken(token) {
      localStorage.setItem('loginToken', token)
    }

    render() {
      return (
        <Formik
          initialValues={{
              name: '',
              email: '',
              password: '',
              confirmPassword: ''
          }}

          validationSchema={Yup.object().shape({
              name: Yup.string()
                .required('First Name is required'),
              email: Yup.string()
                .required('First Name is required'),
              password: Yup.string()
                .required('First Name is required'),
              confirmPassword: Yup.string()
                .required('First Name is required')
          })}

          onSubmit={fields => {
            // axios request to node
            axios.post('http://localhost:4000/addUser', fields)
              .then((response) => {
                if(response.data === "error")
                  console.log(response.data)
                else {
                  console.log(response.data.token)
                  // store this in local storage
                  this.setLocalToken(response.data.token)
                  // redirect to dashboard
                  this.props.history.push('/dashboard')               
                }
              }).catch(err => {
                console.error(err)
            }); //axios ends
          }}
        >
          {({ errors, status, touched }) => (
            <Form>
              <label>Name</label><br/>
              <Field 
                name="name"
                type="text" 
                className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} />
              <ErrorMessage name="name" component="p" className="invalid-feedback" /><br/>
              <label>Email</label><br/>
              <Field 
                name="email"
                type="text" 
                className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
              <ErrorMessage name="email" component="div" className="invalid-feedback" /><br/>
              <label>Password</label><br/>
              <Field 
                name="password"
                type="text" 
                className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
              <ErrorMessage name="password" component="div" className="invalid-feedback" /><br/>
              <label>Confirm password</label><br/>
              <Field 
                name="confirmPassword"
                type="text" 
                className={'form-control' + (errors.confirmPassword && touched.confirmPassword ? ' is-invalid' : '')} />
              <ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" /><br/>
              <button type="submit" className="btn btn-primary mr-2">Register</button>
            </Form>
          )}
      </Formik>
      );
    }
  }

export default Register;