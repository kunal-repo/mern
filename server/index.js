const cors = require('cors')
const express = require('express')
var bodyParser = require('body-parser')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const app = express()
const port = 4000

  
var jsonParser = bodyParser.json()
var db = require('./connection.js')
// app.get('/', (req, res) => res.send('Hello World!'))
/* Select from a table. */

app.use(
    cors({
      origin: 'http://localhost:3000',
      credentials: true
    })
  );


app.post('/addUser', jsonParser, function(req, res) {
    // create user, email and password hash in db
    // create JWT that contains new user id encoded
    // send this token to react
    // console.log(req.body)
    var sql = `INSERT INTO user (name, email, password)
              VALUES ( ?, ?, ? )`;
    db.query(sql, [req.body.name , req.body.email, req.body.password], function (err, data) {
      if (err) {
        res.send("error");
        console.log(err);
      }
      else {
        let userId = data.userId

        // create token with user id
        jwt.sign({userId}, 'mySecretKey', (err, token) => {
          res.json({
            token
          });
          console.log(token)
        });
      }
    });
    // create token and send it to react

    
});

app.get('/', (req, res) => res.send('Hello Worldss!'))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))